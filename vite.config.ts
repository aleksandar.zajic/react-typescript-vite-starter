/// <reference types="vitest" />
/* eslint-disable import/no-extraneous-dependencies */
import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import tsconfigPaths from 'vite-tsconfig-paths';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(), tsconfigPaths()],
  server: {
    port: 3000,
  },
  test: {
    watch: false,
    environment: 'jsdom',
    include: ['./src/**/*.test.ts', './src/**/*.test.tsx'],
    setupFiles: ['./tests/setup.ts'],
    coverage: {
      all: true,
      reporter: ['cobertura', 'lcov', 'text'],
      include: [
        'src/**/*.ts',
        'src/**/*.tsx',
        '!tests/**/*',
        '!**/mocks/**/*.ts',
        '!**/mocks/**/*.tsx',
        '!**/constants.ts',
        '!**/messages.ts',
      ],
    },
  },
});
