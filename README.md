<p align='center'>
Mocking up web app with <b>Vitality</b><sup><em>🫀</em></sup><br>
</p>

<br>

## Features

- ⚡️ [React 18](https://reactjs.org/)
- 🦾 TypeScript, of course
- 🫀 [Vitest](https://vitest.dev/) - unitary testing made easy
- 🗂 [Absolute imports](https://github.com/vitejs/vite/issues/88#issuecomment-762415200)
- TODO: 🎨 [Tailwind with JIT](https://tailwindcss.com/) - next generation utility-first CSS

### Coding Style

- [ESLint](https://eslint.org/) - configured for React/Hooks & TypeScript
- [Prettier](https://prettier.io/)

### Dev tools

- [TypeScript](https://www.typescriptlang.org/)

## Todo Checklist

- [ ] Fix ESLINT
- [ ] Add Tailwind / Chakra UI
- [ ] Clean up the README's

And, enjoy :)

## Usage

### Development

Just run and visit http://localhost:3000

```bash
npm run dev
```

### Build

To build the App, run

```bash
npm run build
```

And you will see the generated file in `dist` that ready to be served.
