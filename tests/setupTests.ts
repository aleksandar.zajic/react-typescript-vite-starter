import { server } from '../__mocks__/msw/server';
import { msw } from 'msw';
import "@testing-library/jest-dom";

beforeAll(() => server.listen({ onUnhandledRequest: 'error' }));
afterAll(() => server.close());
afterEach(() => server.resetHandlers());
