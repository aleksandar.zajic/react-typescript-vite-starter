import { RouterProps } from '@tanstack/react-location';

declare const Routes: (
  props?: Omit<RouterProps, 'children' | 'location' | 'routes'>,
) => JSX.Element;

export default Routes;
