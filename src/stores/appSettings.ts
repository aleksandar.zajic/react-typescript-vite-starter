import create from 'zustand';
import vanillaCreate from 'zustand/vanilla';
import { persist } from 'zustand/middleware';

export const appSettingsStore = vanillaCreate(
  persist(
    (setState) => ({
      appSettings: {},
      currentLocale: 'en-GB',
      setAppSettings: (appSettings) => setState(() => ({ appSettings })),
      setCurrentLocale: (locale) =>
        setState(() => ({
          currentLocale: locale,
        })),
    }),
    {
      name: 'app_settings',
    },
  ),
);

export const useAppSettingsStore = create(appSettingsStore);
