import { LoaderFn, MakeGenerics, useMatch } from '@tanstack/react-location';

type PolicyType = {
  id: string;
};

type Route = MakeGenerics<{ LoaderData: PolicyType; Params: { id: string } }>;

export const Loader: LoaderFn<Route> = ({ params }) => {
  return { id: params.id };
};

export default function Policy() {
  const { data } = useMatch<Route>();

  return (
    <>
      <h1>Edit Policy @ {data.id}</h1>

      <code>
        Loader data
        <pre>{JSON.stringify(data, null, 2)}</pre>
      </code>
    </>
  );
}
